<?php
  class Gestiones extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      if(!$this->session->userdata("usuarioC0nectado")){
          $this->session->set_flashdata("error","Por favor Inicie Sesion");
          redirect('seguridades/cerrarSesion');
      }else{//Codigo cuando SI esta conectado
        if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
          || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
            redirect('seguridades/cerrarSesion');
        }
      }
    }

    public function gestionClientes(){
      $clientes=new grocery_CRUD();
      $clientes->set_table('cliente'); //nombre de la bbdd
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      //$clientes->set_file
      $clientes->display_as("codigo_cli","CÓDIGO");
      $clientes->display_as("cedula_cli","CEDULA");
      $clientes->display_as("nombres_cli","NOMBRE");
      $clientes->display_as("apellidos_cli","APELLIDO");
      $clientes->display_as("direccion_cli","DIRECCION");
      $clientes->display_as("telefono_convencional_cli","TELEFONO CONVENCIONAL");
      $clientes->display_as("telefono_celular_cli","TELEFONO CECULAR");
      $clientes->set_subject("cliente");
      $clientes->columns('cedula_cli','apellidos_cli','nombres_cli');
      $clientes->fields('cedula_cli','apellidos_cli','nombres_cli','direccion_cli','telefono_celular_cli');
      $clientes->required_fields('cedula_cli','apellidos_cli','nombres_cli','telefono_celular_cli','direccion_cli');

      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestiones/gestionClientes',$output);
      $this->load->view('pie');
    }

    public function gestionGeneros(){
      $clientes=new grocery_CRUD();
      $clientes->set_table('genero'); //nombre de la bbdd
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      //$clientes->set_file
      $clientes->display_as("id_gen","CÓDIGO");
      $clientes->display_as("genero_pel","GENERO");

      $clientes->set_subject("cliente");
      $clientes->columns('id_gen','genero_pel');
      $clientes->fields('id_gen','genero_pel');
      $clientes->required_fields('id_gen','genero_pel');

      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestiones/gestionGeneros',$output);
      $this->load->view('pie');
    }


  }
 ?>
