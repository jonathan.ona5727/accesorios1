<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <link rel='SHORTCUT ICON' href='https://st2.depositphotos.com/4435747/9906/v/950/depositphotos_99069320-stock-illustration-mobile-repair-and-development-illustration.jpg' type='image/x-icon' />

    <title>Venta de Accesorios</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <script src="https://code.jquery.com/jquery-3.6.0.js

   "integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="crossorigin="anonymous"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js">
   </script>
   <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Favicon -->
    <link href="<?php echo base_url(); ?>/assets/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Inter:wght@700;800&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url(); ?>/assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">

</script>


        <?php if ($this->session->flashdata('confirmacion')):?>
  	<script type="text/javascript">
  	$(document).ready(function(){
  		Swal.fire(
  		  'CONFIRMACION!',
  			'<?php echo
  			$this->session->flashdata('confirmacion');?>',

  			'success'
  		//  'warning'//icono de advertencia

  		);
  	});
  	</script>
  <?php endif; ?>


        <?php if ($this->session->flashdata('eliminacion')):?>
        <script type="text/javascript">
        Swal.fire({
        	title: 'Are you sure?',
        	text: "You won't be able to revert this!",
        	icon: 'warning',
        	showCancelButton: true,
        	confirmButtonColor: '#3085d6',
        	cancelButtonColor: '#d33',
        	confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        	if (result.isConfirmed) {
        		Swal.fire(
        			'Deleted!',
        			'Your file has been deleted.',
        			'success'
        		)
        	}
        })

        </script>
        <?php endif; ?>
        <?php if ($this->session->flashdata('error')):?>
		<script type="text/javascript">
		$(document).ready(function(){
			Swal.fire(
				'ERROR!',
				'<?php echo
				$this->session->flashdata('error');?>',
				'error'

			);
		});
		</script>
	<?php endif; ?>

  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!-- Importando  file -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/fileinput.min.js" integrity="sha512-CCLv901EuJXf3k0OrE5qix8s2HaCDpjeBERR2wVHUwzEIc7jfiK9wqJFssyMOc1lJ/KvYKsDenzxbDTAQ4nh1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/css/fileinput.min.css" integrity="sha512-qPjB0hQKYTx1Za9Xip5h0PXcxaR1cRbHuZHo9z+gb5IgM6ZOTtIH4QLITCxcCp/8RMXtw2Z85MIZLv6LfGTLiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/locales/es.min.js" integrity="sha512-Mu+FX6b1AzpD49KmO13T8uLDpHK8M1vgZ75094jEP4KV0j59xJFfRz/SP0QIr/9dmHc4yIUGEFcnTU3uhHJaJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <meta name="theme-color" content="#FFA56D">
  <meta name="MobileOptimized" content="width">
  <meta name="HandheldFriendly" content="true">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <link rel="shortcut icon" type="image/png" href="./img/pelicula.png">
  <link rel="apple-touch-icon" href="./pelicula.png">
  <link rel="apple-touch-startup-image" href="./pelicula.png">
  <meta name="apple-mobile-web-app-title" content="Alquiler pelicual">
  <link rel="manifest" href="<?php echo base_url(); ?>manifest.json">

  <script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
  <!-- Importacion de push js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.8/push.js" integrity="sha512-x0GVeKL5uwqADbWOobFCUK4zTI+MAXX/b7dwpCVfi/RT6jSLkSEzzy/ist27Iz3/CWzSvvbK2GBIiT7D4ZxtPg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- Importacion de Push Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.8/push.js" integrity="sha512-x0GVeKL5uwqADbWOobFCUK4zTI+MAXX/b7dwpCVfi/RT6jSLkSEzzy/ist27Iz3/CWzSvvbK2GBIiT7D4ZxtPg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script type="text/javascript">
    function	lanzarNotificacion(){
      Push.Permission.request(); //Solicitar Permiso
      //Creando Notificación Push
      Push.create('NOTIFICACIÓN',{
        body:'Saludo Septimo Sistemas',
        icon:'<?php echo base_url('/img/favicon.png'); ?>',
        timeout:10000, //tiempo de visibilidad en milisegundos
        vibrate: [100,100,100],
        onClick:function(){
          alert("ok");
        }
      });

    }
</script>

</head>

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Navbar Start -->
        <div class="container-fluid nav-bar bg-transparent">
            <nav class="navbar navbar-expand-lg bg-white navbar-light py-0 px-4">
                <a href="<?php echo base_url(); ?>" class="navbar-brand d-flex align-items-center text-center">
                    <div class="icon p-2 me-2">
                        <img class="img-fluid" src="<?php echo base_url(); ?>/assets/img/images.png" alt="Icon" style="width: 35px; height: 35px;">

                    </div>
                    <?php if ($this->session->userdata("usuarioC0nectado")): ?>
               <?php echo $this->session->userdata("usuarioC0nectado")['perfil']; ?>
            <?php endif; ?>

                </a>
                <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">

                    <div class="navbar-nav ms-auto" >
                        <a href="<?php echo site_url()	?>" class="nav-item nav-link active">Inicio</a>
                        <a href="<?php echo site_url()	?>/contactos/index" class="nav-item nav-link">Contact</a>
                        

                         <?php if ($this->session->userdata("usuarioC0nectado")): ?>
                        <a href="<?php echo site_url()	?>/gestiones/gestionClientes" class="nav-item nav-link">Clientes</a>
                         <?php if($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR" ): ?>
                        <a href="<?php echo site_url()	?>/peliculas/index" class="nav-item nav-link">Accesorios</a>
                         <?php endif; ?>

                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Mercaderia</a>
                            <div class="dropdown-menu rounded-0 m-0">
                                <a href="<?php echo site_url()	?>/generos/index" class="dropdown-item">Categoria</a>
                                <a href="<?php echo site_url()	?>/reportes/jo" class="dropdown-item">Reportes</a>
                                <a href="<?php echo site_url()	?>" class="dropdown-item">Property Agent</a>

                            </div>

                        </div>
                        <?php endif; ?>
                        <div class="nav-item dropdown">

                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Usuario</a>
                            <div class="dropdown-menu rounded-0 m-0">
                                <a href="<?php echo site_url()	?>/seguridades/login" class="dropdown-item">Inicio Seccion</a>
                                <a href="<?php echo site_url()	?>/seguridades/index" class="dropdown-item">Resgistrarse</a>

                                <?php if ($this->session->userdata("usuarioC0nectado")): ?>
                                  <a href="<?php echo site_url(); ?>/seguridades/cerrarSesion" class="dropdown-item">
                                     <?php echo $this->session->userdata("usuarioC0nectado")['email']; ?> Salir</a>
                                     <?php endif; ?>
                            </div>
                        </div>

                    </div>
                    <a href="<?php echo site_url()	?>/alquileres/index" class="btn btn-primary px-3 d-none d-lg-flex">Adquirir compra</a>
                </div>




            </nav>
        </div>



        <!-- Navbar End -->
