<center><h3><font color="black"> LISTADO DE ACCESORIOS</font></h3></center><br>

<?php if ($listadoGeneros): ?>
    <table class="table table-responsive table-bordered table-striped table-danger">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">GENERO</th>
        <th class="text-center">ACCIONES</th>

      </tr>
  </thead>
    <tbody>
      <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $generoTemporal->id_gen ?></td>
          <td class="text-center"><?php echo $generoTemporal->genero_pel ?></td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/generos/editar/<?php echo $generoTemporal->id_gen ?>"><img src="<?php echo base_url(); ?>assets/img/icons/pencil1.png" title="Editar" width="25px"></i></a>
            <a href="<?php echo site_url(); ?>/generos/eliminarGenero/<?php echo $generoTemporal->id_gen ?>" onclick="confirmation(event)"><img src="<?php echo base_url(); ?>assets/img/icons/trash1.png" title="Eliminar" width="25px">

          </a>  </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron Articulos registrados
  </div>
<?php endif; ?>
