<div class="container-xxl py-5 bg-dark hero-header mb-5">
                            <div class="container text-center my-5 pt-5 pb-4">
                                <h1 class="display-3 text-white mb-3 animated slideInDown">Gestión de Accesorios</h1>
																<section class="page-top-section set-bg" data-setbg="<?php echo base_url(); ?>">

																		<div class="site-breadcrumb">
																			Inicio</a>  /
																			<span><a href="">Accesorios</span>
																		</div>
																	</div>
																</section>

                            </div>

<!-- Page top section -->

	<!-- Page top end-->
  <section class="games-section">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <legend><h3><FONT COLOR="black">LISTADO DE ACCESORIOS</FONT></h3></legend>

    </div>

  </div>
<br>
<div class="row">
  <div class="col-md-12">
    <?php if ($listadoPeliculas): ?>
      <table class="table "
      id="tbl-peliculas">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">PORTADA</th>
            <th class="text-center">CANTIDAD</th>
            <th class="text-center">CATEGORIA</th>
            <th class="text-center">COSTO DE COMPRA</th>
            <th class="text-center">ACCIONES</th>
          </tr>
      </thead>
        <tbody>
          <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
            <tr>
              <td class="text-center"><?php echo $peliculaTemporal->id_pel ?></td>

              <td class="text-center"><?php echo $peliculaTemporal->director_pel ?></td>
              <td class="text-center">
                <?php if ($peliculaTemporal->imagen_portada_pel!=""): ?>
                  <a target="_blank" href="<?php echo site_url(); ?>/peliculas/imagen/<?php echo $peliculaTemporal->id_pel ?>">
                      <img src="<?php echo base_url('uploads').'/'.$peliculaTemporal->imagen_portada_pel; ?>"
                      title="<?php echo $peliculaTemporal->titulo_pel ?>" width="80px"><br>
                      </a>
                <?php else: ?>
                        N/A
                <?php endif; ?>
              </td>
              <td class="text-center"><?php echo $peliculaTemporal->duracion_pel ?> Articulos</td>
              <td class="text-center"><?php echo $peliculaTemporal->genero_pel ?></td>

              <td class="text-center"><?php echo $peliculaTemporal->costo_alquiler_pel ?> USD</td>
              <td class="text-center">
              <a href="<?php echo site_url(); ?>/peliculas/editar/<?php echo $peliculaTemporal->id_pel ?>"><img src="<?php echo base_url(); ?>assets/img/icons/pencil1.png" title="Editar" width="25px"></a>
              <a href="<?php echo site_url(); ?>/peliculas/eliminarPelicula/<?php echo $peliculaTemporal->id_pel ?>" onclick="confirmation(event)"><img src="<?php echo base_url(); ?>assets/img/icons/trash1.png" title="Eliminar" width="25px">

              </a>  </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <div class="alert alert-danger">
        No se encontraron Accesorios registrados
      </div>
    <?php endif; ?>

  </div>

</div>
