<div class="container-xxl py-5 bg-dark hero-header mb-5">
                            <div class="container text-center my-5 pt-5 pb-4">
                                <h1 class="display-3 text-white mb-3 animated slideInDown">Editar cliente</h1>
																<section class="page-top-section set-bg" data-setbg="">
																	<div class="page-info">
																		<h2>Clientes</h2>
																		<div class="site-breadcrumb">
																			<a href="<?php echo base_url(); ?>">Inicio</a>  /
																			<span>Clientes</span>
																		</div>
																	</div>
																</section>

                            </div>
                        </div>

	<!-- Page top end-->
  <section class="contact-page">
    <div class="container">
<div class="row">
  <div class="col-md-12 text-center">
    <legend><h2 title="Editar" width="30px">
        <font color="black">NUEVO CLIENTE</font></h2><br>
    </legend>
  </div>

<div class="row">
  <div class="col-md-3">
  </div>
    <div class="col-md-9">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente">
      <table class="">
        <tr>
          <td><label for=""><h4><font color="black">Cédula:</font></h4></label></td>
          <td><input type="number" name="cedula_cli" id="cedula_cli" class="form-control"
          value="" placeholder="Ingrese su cedula" required autocomplete="off"></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 1718192023</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">Nombres:</font><h4></label></td>
          <td><input type="nombres" name="nombres_cli" id="nombres_cli" class="form-control"
          value="" placeholder="Ingrese sus nombres" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Juan Carlos</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Apellidos:</font></h4></label></td>
          <td><input type="apellidos" name="apellidos_cli" id="apellidos_cli" class="form-control"
          value="" placeholder="Ingrese sus apellidos" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Marinez Arias</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Dirección Exacta:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="direccion" name="direccion_cli" id="direccion_cli" class="form-control"
          value="" placeholder="Ingrese sus dirección" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Latacunga, Barrio el Ejido Av Simon Rodriguez</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Teléfono Convencional:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="number" name="telefono_convencional_cli" id="telefono_convencional_cli" class="form-control"
          value="" placeholder="Ingrese sus teléfono"></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 032667788</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Teléfono Celular:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="number" name="telefono_celular_cli" id="telefono_celular_cli" class="form-control"
          value="" placeholder="Ingrese sus teléfono" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 0987654321</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      </table><center>
      <button type="submit" name="button" class="btn btn-primary">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
</div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>


<script type="text/javascript">
  $("#frm_nuevo_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_cli:{
        required:true
      },
      nombres_cli:{
        required:true
      },
      direccion_cli:{
        required:true
      },
      telefono_celular_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_cli:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_cli:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_cli:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_cli:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		errorElement : 'span'
  });
</script>
