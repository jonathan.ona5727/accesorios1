<div class="container-xxl py-5 bg-dark hero-header mb-5">
          <div class="container text-center my-5 pt-5 pb-4">
        <h1 class="display-3 text-white mb-3 animated slideInDown">Nueva Accesorios</h1>
							<section class="page-top-section set-bg" data-setbg="">


								</div>
							</section>

          </div>

<!-- Page top section -->
 <section class="blog-page">
    <div class="container">
<div class="row">
  <div class="col-md-4 text-center">
  </div>
<div class="row">
  <div class="col-md-3">
  </div>
    <div class="col-md-9">

    <form class="newsletter-form" enctype="multipart/form-data" action="<?php echo site_url(); ?>/peliculas/guardarPelicula"
			method="post" id="frm_nueva_pelicula">
        <table class="">
          <tr>
						  <h2><font color="black">NUEVO ACCESORIO</font></h2><br>
							<br>
							<br>

                <td><label for=""><h5><font color="black">Nombre:</font></h5></label></td>
                <td><input type="director" name="director_pel" id="director_pel" class="form-control"
                value="" placeholder="Ingrese el director" required></td>
              </tr>
              <tr>
                <td></td>
                <td><br><font color="gray">Ej. Audifonos</font></td>
              </tr>
          <tr>
               <td><label for=""><h5><font color="black">Portada:</font></h5></label></td>
               <td><input type="file" name="imagen_portada_pel" id="imagen_portada_pel"
                  class="form-control"
                  accept="image/jpeg"
               value=""></td>
             </tr>
             <tr>
               <td>&nbsp;</td>
             </tr>
          <tr>
            <td><label for=""><h5><font color="black">Cantidad:</font></h5></label></td>
            <td><input type="number" name="duracion_pel" id="duracion_pel" class="form-control"
            value="" placeholder="Ingrese la  duración de la pelicula" required></td>
          </tr>
          <tr>
            <td></td>
            <td><br><font color="gray">Ej. 120</font></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><label for=""><h5><font color="black">Categoria:</font></h5></label></td>
            <td><select class="form-control" name="fk_id_gen" id="fk_id_gen">
              <option value="">--Seleccione--</option>
              <?php if ($listadoGeneros): ?>
                <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
                  <option value="<?php echo $generoTemporal->id_gen; ?>">
                    <?php echo $generoTemporal->genero_pel; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
                </select></td>
          </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>

          <tr>
            <td>&nbsp;</td>
          </tr>

      <tr>
          <tr>
            <td><label for=""><h5><font color="black">Precio a pagar:</font></h5></label></td>
            <td><input type="number" name="costo_alquiler_pel" id="costo_alquiler_pel" class="form-control"
            value="" placeholder="Ingrese el costo de alquiler" required></td>
          </tr>
          <tr>
            <td></td>
            <td><br><font color="gray">Ej. 17</font></td>
          </tr>
						<tr>
			        <td>&nbsp;</td>
			      </tr>

			          <tr>
          </table><center>
        <button type="submit" name="button" class="btn btn-primary">
          <i class="glyphicon glyphicon-ok"></i>
          Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url(); ?>/peliculas/index" class="btn btn-primary">
          <i class="glyphicon glyphicon-remove"></i>
          Cancelar</a></center>
      </form>
  </div>
  <div class="col-md-3">
  </div>
</div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nueva_pelicula").validate({
  rules:{
    titulo_pel:{
      required:true,
      remote:{
                  url:"<?php echo site_url('peliculas/validarTituloExistente'); ?>",
                  data:{
                    "titulo_pel":function(){
                      return $("#titulo_pel").val();
                    }
                  },
                  type:"post"
              }

    },
    duracion_pel:{
      required:true,
      digits:true,
      maxlength:4
    },
    director_pel:{
      required:true
    },
		fk_id_gen:{
      required:true
		},
    costo_alquiler_pel:{
      required:true,
      digits:true,
      maxlength:3
    }
  },
  messages:{
    titulo_pel:{
      required:"<br>Por favor ingrese el título",
      remote:"Este titulo no se puede duplicar"

    },
    duracion_pel:{
      required:"<br>Por favor ingrese la duración",
      digits:"<br>Por favor ingrese solo numeros",
      maxlength:"<br>Por favor ingrese 4 digitos"
    },
    director_pel:{
      required:"<br>Por favor ingrese el director"
    },
		fk_id_gen:{
      required:"<br>Por favor seleccione el genero"
		},
    costo_alquiler_pel:{
      required:"<br>Por favor ingrese el costo del alquiler",
      digits:"<br>Por favor ingrese solo numeros",
      maxlength:"<br>Por favor ingrese 3 digitos"
    }
  },
	errorElement : 'span'
});
</script>

<script type="text/javascript">
  $(document).ready(function (){
    $("#imagen_portada_pel").fileinput({
      language:'es'
    });
  });
</script>

<script>
  $( function() {
    var availableTags = [
			<?php if ($listadoDirectores): ?>
				<?php foreach ($listadoDirectores->result() as $directorTemporal): ?>
					'<?php echo $directorTemporal->director_pel; ?>',
				<?php endforeach; ?>
			<?php endif; ?>
    ];
    $( "#director_pel" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
