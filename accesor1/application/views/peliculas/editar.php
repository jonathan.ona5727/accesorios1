
<div class="container-xxl py-5 bg-dark hero-header mb-5">
                            <div class="container text-center my-5 pt-5 pb-4">
                                <h1 class="display-3 text-white mb-3 animated slideInDown">Editar Accesorio</h1>
																<section class="page-top-section set-bg" data-setbg="<?php echo base_url(); ?>">

																		<div class="site-breadcrumb">
																			Inicio</a>  /
																			<span><a href="">Accesorios</span>
																		</div>
																	</div>
																</section>

                            </div>
<!-- Page top section -->

	<!-- Page top end-->
  <section class="blog-page">
    <div class="container">
<div class="row">
  <div class="col-md-12 text-center">
    <legend><h2>
        <img src="<?php echo base_url(); ?>assets/img/icons/plus.png" title="Editar" width="30px">
        <font color="black">EDITAR ACCESORIOS</font></h2><br>
    </legend>
  </div>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" enctype="multipart/form-data" action="<?php echo site_url(); ?>/peliculas/actualizarPelicula" method="post" id="frm_editar_pelicula">
      <input type="hidden" name="id_pel" id="id_pel" class="form-control"
      value="<?php echo $peliculaEditar->id_pel; ?>" placeholder="Ingrese su id">
			<input type="hidden" name="id_img" id="id_img" class="form-control"
      value="<?php echo $peliculaEditar->imagen_portada_pel; ?>" placeholder="Ingrese su id">
      <table class="">
        <tr>
          <td><label for=""><h4><font color="black">Título:</font></h4></label></td>
          <td><input type="titulo" name="titulo_pel" id="titulo_pel" class="form-control"
          value="<?php echo $peliculaEditar->titulo_pel; ?>" placeholder="Ingrese su titulo" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Avatar</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">Duración:</font></h4></label></td>
          <td><input type="number" name="duracion_pel" id="duracion_pel" class="form-control"
          value="<?php echo $peliculaEditar->duracion_pel; ?>" placeholder="Ingrese su duración" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 120</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">Director:</font></h4></label></td>
          <td><input type="director" name="director_pel" id="director_pel" class="form-control"
          value="<?php echo $peliculaEditar->director_pel; ?>" placeholder="Ingrese el director" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. James Cameron</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">Género:</font></h4></label></td>
          <td><select class="form-control" name="fk_id_gen" id="fk_id_gen">
            <option value="">--Seleccione--</option>
            <?php if ($listadoGeneros): ?>
              <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
                <option value="<?php echo $generoTemporal->id_gen; ?>">
                  <?php echo $generoTemporal->genero_pel; ?>
                </option>
              <?php endforeach; ?>
            <?php endif; ?>
              </select></td>
            </tr>

        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
            <tr>
          <td><label for=""><h4><font color="black">Costo Alquiler:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="number" name="costo_alquiler_pel" id="costo_alquiler_pel" class="form-control"
          value="<?php echo $peliculaEditar->costo_alquiler_pel; ?>" placeholder="Ingrese el costo de alquiler" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 17</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
					<tr>
						<td><label for=""><h4><font color="black">Portada:</font></h4></label></td>
						<td><input type="file" name="imagen_portada_pel" id="imagen_portada_pel" class="form-control"
						value="" required></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>

      </table><center>
      <button type="submit" name="button" class="btn btn-primary">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/peliculas/index" class="btn btn-danger">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
  <div class="col-md-3">
  </div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
	$("#fk_id_gen").val('<?php echo $peliculaEditar->fk_id_gen; ?>');
</script>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_editar_pelicula").validate({
		rules:{
	    titulo_pel:{
	      required:true
	    },
	    duracion_pel:{
	      required:true,
	      digits:true,
	      maxlength:4
	    },
	    director_pel:{
	      required:true
	    },
			fk_id_gen:{
	      required:true
			},
	    costo_alquiler_pel:{
	      required:true,
	      digits:true,
	      maxlength:3
	    }
	  },
	  messages:{
	    titulo_pel:{
	      required:"<br>Por favor ingrese el título"
	    },
	    duracion_pel:{
	      required:"<br>Por favor ingrese la duración",
	      digits:"<br>Por favor ingrese solo numeros",
	      maxlength:"<br>Por favor ingrese 4 digitos"
	    },
	    director_pel:{
	      required:"<br>Por favor ingrese el director"
	    },
			fk_id_gen:{
	      required:"<br>Por favor seleccione el genero"
			},
	    costo_alquiler_pel:{
	      required:"<br>Por favor ingrese el costo del alquiler",
	      digits:"<br>Por favor ingrese solo numeros",
	      maxlength:"<br>Por favor ingrese 3 digitos"
	    }
	  },
		errorElement : 'span'
	});
	</script>
