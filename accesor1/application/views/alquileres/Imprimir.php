<br><br><br><br>

<section class="">
      <title>Accesrios</title>

      <!-- Favicon -->
      <link rel="icon" href="<?php echo base_url(); ?>/assets/img/core-img/favicon.ico">

      <!-- Stylesheet -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets\style.css">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="https://code.jquery.com/jquery-3.6.0.js"integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="crossorigin="anonymous"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
  <!-- jsdelivr es un cdn para los mensajes de confirmacion -->
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <div class="bradcumbContent">

          <center>  <h2>FACTURA</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

   <!-- ##### Login Area Start ##### -->
   <section class="login-area section-padding-100">
       <div class="container">
           <div class="row justify-content-center">
             <div class="col-md-2">

             </div>
               <div class="col-md-8 ">
                 <div class="col-3">

                 </div>
                   <div class="login-content"> <center>
                     <form class=""  id="fmContrato"method="post" action="<?php echo site_url()?>/alquileres/actualizarAlquiler">
                     <div class="row">

                       <div class="col-md-4">
                         <table class="">
                           <tr>
                             <td><label for=""><h4><font color="black">FECHA COMPRA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
                             <td><input type="date" name="fecha_inicio_alqui" id="fecha_inicio_alqui" value="<?php echo $alquilerEditar->fecha_inicio_alqui; ?>" disabled class="form-control"></td>
                           </tr>
                           <tr>
                             <td>&nbsp;</td>
                           </tr>

                             <tr>
                               <td><label for=""><h4><font color="black">HORA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
                               <td><input type="time" name="hora_alqui" id="hora_alqui" value="<?php echo $alquilerEditar->hora_alqui; ?>" disabled class="form-control"></td>
                             </tr>
                             <tr>
                               <td>&nbsp;</td>
                             </tr>
                             <tr>
                             <td><label for=""><h4><font color="black">CLIENTE:</font></h4></label></td>
                             <td><select class="form-control " name="fk_id_cli" id="fk_id_cli" disabled>
                                 <option value="">--Seleccione--</option>
                                 <?php if ($listadoClientes): ?>
                                   <?php foreach ($listadoClientes->result() as $key => $clienteTemporal): ?>
                                     <option value="<?php echo $clienteTemporal->id_cli; ?>">
                                       <?php echo $clienteTemporal->cedula_cli; ?> - <?php echo $clienteTemporal->apellidos_cli; ?> <?php echo $clienteTemporal->nombres_cli; ?>
                                     </option>
                                   <?php endforeach; ?>
                                 <?php endif; ?>
                             </select></td>
                           </tr>
                           <tr>
                             <td>&nbsp;</td>
                           </tr>
                           <tr>
                             <td><label for=""><h4><font color="black">CANTIDAD:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
                             <td><input type="number" name="cantidad_alqui" id="cantidad_alqui" value="<?php echo $alquilerEditar->cantidad_alqui; ?>" disabled class="form-control"></td>
                           </tr>
                           <tr>
                             <td>&nbsp;</td>
                           </tr>
                           <tr>
                             <td><label for=""><h4><font color="black">ACCESORIOS:</font></h4></td>
                             <td><select class="form-control" name="fk_id_pel" id="fk_id_pel" disabled>
                                 <option value="">--Seleccione--</option>
                                 <?php if ($listadoPeliculas): ?>
                                   <?php foreach ($listadoPeliculas->result() as $key => $peliculaTemporal): ?>
                                     <option value="<?php echo $peliculaTemporal->id_pel; ?>">
                                       <?php echo $peliculaTemporal->director_pel; ?> - <?php echo $peliculaTemporal->costo_alquiler_pel; ?> USD
                                     </option>
                                   <?php endforeach; ?>
                                 <?php endif; ?>
                             </select></td>
                           </tr>
                           <tr>
                             <td>&nbsp;</td>
                           </tr>
                           <tr>
                             <td><label for=""><h4><font color="black">PRECIO A COMPRAR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
                             <td><input type="number" name="preciot_alqui" id="preciot_alqui" value="<?php echo $alquilerEditar->preciot_alqui; ?>" disabled class="form-control"></td>
                           </tr>
                           <tr>
                             <td>&nbsp;</td>
                           </tr>


                                 <br>
                       </table>
  <div class="col-md-12 text-center">
    <button type="button" name="button" class="btn btn-primary" style="background-color:#17BB0F" onclick="imprimir();">
      Imprimir
    </button>
    <a href="<?php echo site_url()?>/Alquileres/index"  class="btn btn-danger">
      Regresar
    </a>
  </div>

</div>
</form>
</div>
<div class="col-md-3 text-center" >
</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
// Colocamos en el imput select los valores que tomamos de la cadena alquilerEditar con el id que proporciona el usuario
$("#fk_id_cli").val('<?php echo $alquilerEditar->fk_id_cli;?>');
$("#fk_id_pel").val('<?php echo $alquilerEditar->fk_id_pel;?>');

</script>
<iframe id="if_print" style="display:none">

</iframe>

<script  type="text/javascript">
  function imprimir(){
    document.getElementById('if_print').src = '<?php echo site_url("facturas/imprimir"); ?>';
    print();
  }

</script>
