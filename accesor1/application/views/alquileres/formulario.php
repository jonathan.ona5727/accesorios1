<div class="container-xxl py-5 bg-dark hero-header mb-5">
          <div class="container text-center my-5 pt-5 pb-4">
        <h1 class="display-3 text-white mb-3 animated slideInDown">Formulario alquiler</h1>
							<section class="page-top-section set-bg" data-setbg="">


								</div>
							</section>

          </div>
	<!-- Page top end-->
  <section class="blog-page">
    <div class="container">
			<div class="row">
  <div class="col-md-12 text-center">
  <legend><h3><font color="black" width="30px">
    FORMULARIO DE ALQUILERES</font></h3><br></legend>

  </div>
<div class="row">
	<div class="col-md-3">
  </div>
    <div class="col-md-9">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/alquileres/insertarAlquiler" method="post" id="frm_nuevo_alquiler">
      <table class="">
        <tr>
          <td><label for=""><h4><font color="black">FECHA COMPRA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="date" name="fecha_inicio_alqui" id="fecha_inicio_alqui" value="" required class="form-control" ></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

          <tr>
            <td><label for=""><h4><font color="black">HORA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
            <td><input type="time" name="hora_alqui" id="hora_alqui" value="" required class="form-control"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
          <td><label for=""><h4><font color="black">CLIENTE:</font></h4></label></td>
          <td><select class="form-control " name="fk_id_cli" id="fk_id_cli" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoClientes): ?>
                <?php foreach ($listadoClientes->result() as $clienteTemporal): ?>
                  <option value="<?php echo $clienteTemporal->id_cli; ?>">
                    <?php echo $clienteTemporal->cedula_cli; ?>-<?php echo $clienteTemporal->apellidos_cli; ?><?php echo $clienteTemporal->nombres_cli; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">CANTIDAD:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="number" name="cantidad_alqui" id="cantidad_alqui" value="" required class="form-control" required></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">ACCESORIOS:</font></h4></td>
          <td><select class="form-control" name="fk_id_pel" id="fk_id_pel" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoPeliculas): ?>
                <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
                  <option value="<?php echo $peliculaTemporal->id_pel; ?>">
                    <?php echo $peliculaTemporal->director_pel; ?> - <?php echo $peliculaTemporal->costo_alquiler_pel; ?> USD
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">PRECIO A COMPRAR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="number" name="preciot_alqui" id="preciot_alqui" value="" required class="form-control" required></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td><label for=""><h4><font color="black">ESTADO DE COMPRA:</font></h4></td>
          <td><select class="form-control" name="estado_alqui" id="estado_alqui" required>
              <option value="">--Seleccione--</option>
              <option value="Pendiente">Pendiente</option>
              <option value="Pagado">Pagado</option>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>


              <br>
    </table>
		<center>
    <button type="submit" name="button" class="btn btn-primary">GUARDAR ALQUILER</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>" class="btn btn-danger">CANCELAR</a>


    </center>
	</form>
	</div>
	<div class="col-md-3">

	</div>
	</div>
	</div>

	</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nuevo_alquiler").validate({
    rules:{
			fk_id_cli:{
        required:true,
      },
			fk_id_pel:{
        required:true,
      },
      fecha_inicio_alqui:{
        required:true,
      },
			fecha_fin_alqui:{
        required:true,
      }
    },
    messages:{
			fk_id_cli:{
				required:"<br>Por favor seleccione el cliente",
			},
			fk_id_pel:{
				required:"<br>Por favor seleccione la pelicula",
			},
      fecha_inicio_alqui:{
        required:"<br>Por favor ingrese la fecha de inicio de alquiler",
      },
			fecha_fin_alqui:{
        required:"<br>Por favor ingrese la fecha de fin de alquiler",
      }
    },
      errorElement : 'span'
  });
</script>
