<div class="container-xxl py-5 bg-dark hero-header mb-5">
          <div class="container text-center my-5 pt-5 pb-4">
        <h1 class="display-3 text-white mb-3 animated slideInDown">Listado de Generos</h1>
							<section class="page-top-section set-bg" data-setbg="">

									<div class="site-breadcrumb">
									Inicio</a>  /
										<span>	<a href="">Generos</span>
										</div>
								</div>
							</section>

          </div>

<?php
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />

<?php endforeach; ?>


<?php foreach($js_files as $file): ?>

<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>



   <div class="row" style="padding:0px !important; margin:0px !important;">
      <div class="col-md-12 text-center">
         <div class="section-title" data-aos="fade-right">
            <div class="well">
                  <h3><b>GESTIÓN DE GENEROS</b> </h3>
              </div>
            </div>
         </div>
      </div>

   <div >
      <?php echo $output; ?>
   </div>


</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
