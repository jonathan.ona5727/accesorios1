<?php
  class Pelicula extends CI_Model{
    //funcion para inserta nuevo pelicula
    public function insertar($datosPelicula){
      return $this->db->insert('pelicula',$datosPelicula);
    }
    //funcion para consultar datos de BDD
    public function obtenerTodos(){
      $query=$this->db->select('*','genero_pel')
                      ->from('pelicula')
                      ->join("genero", "genero.id_gen = pelicula.fk_id_gen")
                      ->get();
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function obtenerTodosPorGenero1(){
            $query=$this->db->select('genero_pel, COUNT(fk_id_gen) as conteo')
                            ->from('pelicula')
                            ->join("genero", "genero.id_gen = pelicula.fk_id_gen")
                            ->group_by('fk_id_gen')
                            ->get();
            if ($query->num_rows()>0) {
              return $query;//cuando si hay registros en la BDD
            }else {
              return false;//cuando no hay registros
            }
          }

          public function obtenerTodosDirectoresConteo(){
            $query=$this->db->select('director_pel, COUNT(director_pel) as conteo')
                            ->from('pelicula')
                            ->group_by('director_pel')
                            ->get();
            if ($query->num_rows()>0) {
              return $query;//cuando si hay registros en la BDD
            }else {
              return false;//cuando no hay registros
            }
          }
    //funcion para consultar datos de BDD por id
    public function obtenerPorId($id){
      $this->db->where('id_pel',$id);
      $query=$this->db->get('pelicula');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //metodo para eliminar peliculas recibiendo
    //como parametro su idea
    //funcion para eliminar pelicula
  public function eliminarPorId($id)
  {
    $this->db->where('id_pel',$id);
    return $this->db->delete('pelicula');
  }
  //funcion para procesar la actualizacion del usuarios
  public function actualizar($id,$datosPeliculas){
    $this->db->where('id_pel',$id);
    return $this->db->update('pelicula',$datosPeliculas);
  }
  public function consultarTituloPorCedula($titulo_pel) {
      $this->db->where('titulo_pel',$titulo_pel);
      $query=$this->db->get('pelicula');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }

  }
  }
 ?>
